package com.peterchaschke.imageservices.model;

import java.time.Instant;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@RequiredArgsConstructor
public class ImageDTO {
	
	private Long id;
	private String path;
	private String thumbnailPath;
	private Instant timestamp;
	private Long locationId;
	private Long camId;
	private String imageName;
	private String imageThumbnailName;
	private Boolean isRetained = false;
	
}
