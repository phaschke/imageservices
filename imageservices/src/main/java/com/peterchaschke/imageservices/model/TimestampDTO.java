package com.peterchaschke.imageservices.model;

import java.time.Instant;
import java.time.LocalDateTime;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TimestampDTO {
	
	Instant instant;
	LocalDateTime localDateTime;
}
