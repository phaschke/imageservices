package com.peterchaschke.imageservices.model;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IncomingFileDetailsRequestDTO {
	
	private String timestamp;
	private String user;
	private String password;
	private int motionSensed;
	
	public FileDetailsRequestDTO toDTO() {
		
		FileDetailsRequestDTO fileDetailsRequestDTO = new FileDetailsRequestDTO();
		
		if(this.timestamp != null) {
			
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-M-d H:m:s");
			LocalDateTime ldt = LocalDateTime.parse(this.timestamp, formatter);

			ZoneId zoneId = ZoneId.of("Etc/GMT");
			Instant timestamp = ldt.atZone(zoneId).toInstant();
			fileDetailsRequestDTO.setTimestamp(timestamp);
		}
		fileDetailsRequestDTO.setUser(this.getUser());
		fileDetailsRequestDTO.setPassword(this.getPassword());
		fileDetailsRequestDTO.setMotionSensed(this.getMotionSensed());
		
		return fileDetailsRequestDTO;
	}
}
