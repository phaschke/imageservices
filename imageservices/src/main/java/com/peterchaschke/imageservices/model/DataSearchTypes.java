package com.peterchaschke.imageservices.model;

public enum DataSearchTypes {
	ALL,
	ONLY_RETAINED,
	ONLY_NON_RETAINED
}
