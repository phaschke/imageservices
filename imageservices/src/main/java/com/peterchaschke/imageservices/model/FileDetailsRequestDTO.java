package com.peterchaschke.imageservices.model;

import java.time.Instant;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@RequiredArgsConstructor
public class FileDetailsRequestDTO {
	
	private Instant timestamp;
	private String user;
	private String password;
	private int motionSensed;
	
}
