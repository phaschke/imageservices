package com.peterchaschke.imageservices.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@RequiredArgsConstructor
public class LocationWithAuthDTO extends LocationDTO {
	
	private String user;
	private String userKey;
}
