package com.peterchaschke.imageservices.model;

import com.peterchaschke.imageservices.entity.Location;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@RequiredArgsConstructor
public class CamDTO {
	
	private Long id;
	private String name;
	private boolean active;
	private Roles allowedRole;
	private Location location;

}
