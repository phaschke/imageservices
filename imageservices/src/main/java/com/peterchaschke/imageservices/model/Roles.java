package com.peterchaschke.imageservices.model;

public enum Roles {
	ROLE_ADMIN,
	ROLE_USER,
	ROLE_GUEST
}
