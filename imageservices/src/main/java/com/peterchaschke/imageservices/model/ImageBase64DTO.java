package com.peterchaschke.imageservices.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ImageBase64DTO {
	
	String image;
	ImageDTO imageDetails;
}
