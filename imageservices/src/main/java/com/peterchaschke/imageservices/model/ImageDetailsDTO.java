package com.peterchaschke.imageservices.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ImageDetailsDTO {
	
	private ImageDTO imageDTO;
	private CamDTO camDTO;
	private String image;

}
