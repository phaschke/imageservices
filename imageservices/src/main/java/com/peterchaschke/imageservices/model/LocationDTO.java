package com.peterchaschke.imageservices.model;

import java.util.Collection;

import com.peterchaschke.imageservices.entity.Cam;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@RequiredArgsConstructor
public class LocationDTO {
	
	private Long id;
	private Long weatherStationId;
	private String timezone;
	private String name;
	private boolean active;
	private Collection<Cam> cams;
}