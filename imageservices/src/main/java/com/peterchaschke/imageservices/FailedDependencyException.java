package com.peterchaschke.imageservices;

public class FailedDependencyException extends RuntimeException {

	private static final long serialVersionUID = 5929328046408161696L;
	
	public FailedDependencyException(String message) {
		super(message);
	}
}
