package com.peterchaschke.imageservices.storage;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Stream;

import com.peterchaschke.imageservices.model.ImageDTO;
import com.peterchaschke.imageservices.model.ThumbnailDTO;
import com.peterchaschke.imageservices.model.TimestampDTO;

public interface StorageService {

	void init();

	Path getPath(TimestampDTO timestampDTO) throws IOException;

	String save(InputStream image, Path uploadPath, String imageName) throws IOException;

	Stream<Path> loadAll();

	Path load(String filename);

	byte[] loadImage(ImageDTO imageDTO) throws IOException;
	
	public byte[] loadImageThumbnail(ImageDTO imageDTO) throws IOException;
	
	public List<ThumbnailDTO> loadThumbnails(List<ThumbnailDTO> thumbnails) throws IOException;
	
	public void deleteImageAndThumbnail(ImageDTO imageDTO) throws IOException;
	
	public boolean imageExists(ImageDTO imageDTO);
	
	public List<Path> listFiles() throws IOException;

	void deleteByPath(Path path) throws IOException;
	
	public void removeEmptyDirectories() throws IOException;

}
