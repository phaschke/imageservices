package com.peterchaschke.imageservices.storage;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import com.peterchaschke.imageservices.model.ImageDTO;
import com.peterchaschke.imageservices.model.ThumbnailDTO;
import com.peterchaschke.imageservices.model.TimestampDTO;

@Service
public class FilesStorageServiceImpl implements StorageService {

	@Value("${filesystem.root.dir}")
	private String fileSystemRootStr;
	
	private static final Logger logger = LoggerFactory.getLogger(FilesStorageServiceImpl.class);

	private Path root = null;

	@Override
	public void init() {

		Path root = Paths.get(fileSystemRootStr);
		this.root = root;

		try {
			Files.createDirectories(root);
		} catch (IOException e) {
			throw new RuntimeException("Could not initialize folder for upload!");
		}
	}

	@Override
	public Path getPath(TimestampDTO timestampDTO) throws IOException {

		StringBuilder sb = new StringBuilder();
		sb.append(root.toString());
		sb.append(File.separator);
		sb.append(timestampDTO.getLocalDateTime().getYear());
		sb.append(File.separator);
		sb.append(timestampDTO.getLocalDateTime().getMonthValue());
		sb.append(File.separator);
		sb.append(timestampDTO.getLocalDateTime().getDayOfMonth());

		String uploadPathStr = sb.toString();

		Path path = Paths.get(uploadPathStr);

		try {
			Files.createDirectories(path);

		} catch (IOException e) {
			throw new RuntimeException("Could not create folder for upload");
		}

		return path;
	}

	@Override
	public String save(InputStream image, Path uploadPath, String imageName) throws IOException {

		try {
			Files.copy(image, uploadPath.resolve(imageName));

		} catch (Exception e) {
			throw new RuntimeException("Could not store the file. Error: " + e.getMessage());
		}

		return uploadPath.toString();
	}

	@Override
	public Stream<Path> loadAll() {
		try {
			return Files.walk(this.root, 1).filter(path -> !path.equals(this.root)).map(this.root::relativize);
		} catch (IOException e) {
			throw new RuntimeException("Could not load the files!");
		}
	}

	@Override
	public Path load(String filename) {
		try {
			Path file = root.resolve(filename);
			Resource resource = new UrlResource(file.toUri());

			if (resource.exists() || resource.isReadable()) {
				return (Path) resource;
			} else {
				throw new RuntimeException("Could not read the file");
			}
		} catch (MalformedURLException e) {
			throw new RuntimeException("Error: " + e.getMessage());
		}
	}

	@Override
	public byte[] loadImage(ImageDTO imageDTO) throws IOException {

		Path path = getFilePath(imageDTO);

		byte[] imageArray = Files.readAllBytes(path);

		return imageArray;
	}

	private Path getFilePath(ImageDTO imageDTO) {

		StringBuilder sb = new StringBuilder();
		sb.append(imageDTO.getPath());
		sb.append(File.separator);
		sb.append(imageDTO.getImageName());

		Path path = Paths.get(sb.toString());

		return path;
	}

	@Override
	public byte[] loadImageThumbnail(ImageDTO imageDTO) throws IOException {

		Path path = getThumbnailFilePath(imageDTO);
		byte[] imageArray = Files.readAllBytes(path);

		return imageArray;

	}

	@Override
	public List<ThumbnailDTO> loadThumbnails(List<ThumbnailDTO> thumbnails) throws IOException {

		List<ThumbnailDTO> thumbnailDTOs = new ArrayList<ThumbnailDTO>();

		for (int i = 0; i < thumbnails.size(); i++) {
			
			ThumbnailDTO thumbnailDTO = thumbnails.get(i);

			if(thumbnailDTO.getImageDTO() != null) {
				byte[] imageArray = loadImageThumbnail(thumbnailDTO.getImageDTO());
				
				//byte[] encoded = Base64.getEncoder().encode(imageArray);
				String encoded = Base64.getEncoder().encodeToString(imageArray);
				
				//thumbnailDTO.setThumbnail(imageArray);
				thumbnailDTO.setThumbnail(encoded);
				
			} else {
				thumbnailDTO.setThumbnail(null);
			}

			thumbnailDTOs.add(thumbnailDTO);
		}

		return thumbnailDTOs;
	}

	private Path getThumbnailFilePath(ImageDTO imageDTO) {

		StringBuilder sb = new StringBuilder();
		sb.append(imageDTO.getPath());
		sb.append(File.separator);
		sb.append(imageDTO.getImageThumbnailName());

		Path path = Paths.get(sb.toString());

		return path;
	}
	
	@Override
	public void deleteImageAndThumbnail(ImageDTO imageDTO) throws IOException {
		
		Path imagePath = getFilePath(imageDTO);
		Path thumbnailPath = getThumbnailFilePath(imageDTO);
		
		FileSystemUtils.deleteRecursively(imagePath);
		FileSystemUtils.deleteRecursively(thumbnailPath);
		
		logger.info(String.format("Deleted file: %s", imagePath));
		logger.info(String.format("Deleted file: %s", thumbnailPath));
		
	}
	
	@Override
	public void deleteByPath(Path path) throws IOException {
		
		FileSystemUtils.deleteRecursively(path);
	}
	
	@Override
	public boolean imageExists(ImageDTO imageDTO) {
		
		Path imagePath = getFilePath(imageDTO);
		
		return Files.exists(imagePath);
	}
	
	@Override
	public List<Path> listFiles() throws IOException {

        List<Path> result;
        try (Stream<Path> walk = Files.walk(root)) {
            result = walk.filter(Files::isRegularFile)
                    .collect(Collectors.toList());
        }
        return result;
    }
	
	@Override
	public void removeEmptyDirectories() throws IOException {
		Files.walk(root)
	        .sorted(Comparator.reverseOrder())
	        .map(Path::toFile)
	        .filter(File::isDirectory)
	        .forEach(File::delete);
	}

}
