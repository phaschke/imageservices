package com.peterchaschke.imageservices.entity;

import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "locations")
@Getter
@Setter
@RequiredArgsConstructor
public class Location {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;
	
	@JsonIgnore
	@Column(name = "user")
	private String user;
	
	@JsonIgnore
	@Column(name = "user_key")
	private String userKey;
	
	@Column(name = "weather_station_id")
	private Long weatherStationId;
	
	@Column(name = "timezone")
	private String timezone;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "active")
	private boolean active;
	
	@JsonBackReference
	@OneToMany
	@JoinTable( 
	    name = "locations_cams", 
	    joinColumns = @JoinColumn(
	      name = "location_id", referencedColumnName = "id"), 
	    inverseJoinColumns = @JoinColumn(
	      name = "cam_id", referencedColumnName = "id")) 
	private Collection<Cam> cams;

}
