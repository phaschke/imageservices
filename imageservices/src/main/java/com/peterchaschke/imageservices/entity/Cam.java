package com.peterchaschke.imageservices.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.peterchaschke.imageservices.model.Roles;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "cams")
@Getter
@Setter
@RequiredArgsConstructor
public class Cam {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "active")
	private boolean active;
	
	@Column(name = "allowed_role")
	@Enumerated(EnumType.STRING)
	private Roles allowedRole;
	
	@JsonBackReference
	@ManyToOne
	private Location location;

}
