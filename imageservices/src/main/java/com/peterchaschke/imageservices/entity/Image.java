package com.peterchaschke.imageservices.entity;

import java.time.Instant;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "images")
@Getter
@Setter
@RequiredArgsConstructor
public class Image {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "path")
	private String path;
	
	@Column(name = "thumbnail_path")
	private String thumbnailPath;

	@Column(name = "timestamp")
	private Instant timestamp;

	private Long locationId;
	
	private Long camId;

	@Column(name = "image_name")
	private String imageName;

	@Column(name = "image_thumbnail_name")
	private String imageThumbnailName;
	
	@Column(name = "retained")
	private Boolean isRetained = false;
	
}
