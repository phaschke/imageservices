package com.peterchaschke.imageservices;

public class UserPermissionsException extends RuntimeException {

	private static final long serialVersionUID = 3270778785679337143L;
	
	public UserPermissionsException(String string) {
		super(string);
	}
}
