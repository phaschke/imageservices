package com.peterchaschke.imageservices.service;

import java.io.IOException;

import org.springframework.security.access.prepost.PreAuthorize;

import com.peterchaschke.imageservices.model.LocationDTO;

public interface ImageService {
	
	@PreAuthorize("hasRole('ADMIN')")
	public void retainImage(Long imageId) throws Exception;
	
	@PreAuthorize("hasRole('ADMIN')")
	public void unretainImage(Long imageId) throws Exception;
	
	@PreAuthorize("hasRole('ADMIN')")
	public LocationDTO deleteImage(Long imageId) throws IOException;


}
