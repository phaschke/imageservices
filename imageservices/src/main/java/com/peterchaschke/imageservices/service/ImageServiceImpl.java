package com.peterchaschke.imageservices.service;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.imageio.ImageIO;

import org.imgscalr.Scalr;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.peterchaschke.imageservices.FailedDependencyException;
import com.peterchaschke.imageservices.UserAuthorizationException;
import com.peterchaschke.imageservices.UserPermissionsException;
import com.peterchaschke.imageservices.entity.Cam;
import com.peterchaschke.imageservices.entity.Image;
import com.peterchaschke.imageservices.entity.Location;
import com.peterchaschke.imageservices.mapper.CamMapper;
import com.peterchaschke.imageservices.mapper.ImageMapper;
import com.peterchaschke.imageservices.mapper.LocationMapper;
import com.peterchaschke.imageservices.model.CamDTO;
import com.peterchaschke.imageservices.model.DataSearchTypes;
import com.peterchaschke.imageservices.model.FileDetailsRequestDTO;
import com.peterchaschke.imageservices.model.ImageBase64DTO;
import com.peterchaschke.imageservices.model.ImageDTO;
import com.peterchaschke.imageservices.model.ImageDetailsDTO;
import com.peterchaschke.imageservices.model.IncomingFileDetailsRequestDTO;
import com.peterchaschke.imageservices.model.LocationDTO;
import com.peterchaschke.imageservices.model.LocationWithAuthDTO;
import com.peterchaschke.imageservices.model.PageableWrapper;
import com.peterchaschke.imageservices.model.Roles;
import com.peterchaschke.imageservices.model.ThumbnailDTO;
import com.peterchaschke.imageservices.model.TimestampDTO;
import com.peterchaschke.imageservices.repo.CamRepository;
import com.peterchaschke.imageservices.repo.ImageRepository;
import com.peterchaschke.imageservices.storage.StorageService;

@Service
public class ImageServiceImpl implements ImageService {

	@Autowired
	private ImageRepository imageRepository;

	@Autowired
	private StorageService storageService;

	@Autowired
	private LocationService locationService;

	@Autowired
	private AppUserDetailsServiceImpl appUserService;

	@Autowired
	private CamRepository camRepository;

	@Autowired
	private CamService camService;

	@Autowired
	private ImageMapper imageMapper;

	@Autowired
	private LocationMapper locationMapper;

	@Autowired
	private CamMapper camMapper;

	public ImageDTO saveImage(Long locationId, Long camId, MultipartFile file, IncomingFileDetailsRequestDTO fileDetails)
			throws Exception, FailedDependencyException, UserAuthorizationException {

		FileDetailsRequestDTO fileDetailsRequestDTO = fileDetails.toDTO();

		Location location = locationService.getLocation(locationId);

		LocationWithAuthDTO locationDTO = locationMapper.toDTOWithAuth(location);

		Cam cam = camService.getActiveCamById(camId);

		if (cam.getLocation().getId() != locationDTO.getId()) {
			throw new FailedDependencyException("Requested cam is not associated with location");
		}

		locationService.authorizeDataRequest(fileDetailsRequestDTO.getUser(), fileDetailsRequestDTO.getPassword(),
				locationDTO);

		TimestampDTO timestampDTO = generateTimestamp(locationDTO);

		ImageDTO imageDTO = new ImageDTO();
		imageDTO.setLocationId(location.getId());
		imageDTO.setCamId(cam.getId());
		imageDTO.setImageName(generateImageName(locationId, camId, timestampDTO, file.getContentType(), false));
		imageDTO.setImageThumbnailName(generateImageName(locationId, camId, timestampDTO, file.getContentType(), true));

		imageDTO.setTimestamp(timestampDTO.getInstant());
		if (fileDetails.getTimestamp() != null) {
			imageDTO.setTimestamp(fileDetailsRequestDTO.getTimestamp());
		}

		InputStream thumbnailStream = createThumbnail(file, 250);
		InputStream imageStream = file.getInputStream();
		Path imageUploadPath = null;
		try {
			imageUploadPath = storageService.getPath(timestampDTO);

		} catch (IOException e) {

			imageStream.close();
			thumbnailStream.close();
		}

		Image savedImage = persistImage(imageStream, thumbnailStream, imageUploadPath, imageDTO);
		
		return imageMapper.toDTOWithoutPaths(savedImage);
	}

	@Transactional
	public Image persistImage(InputStream imageStream, InputStream thumbnailStream, Path imageUploadPath,
			ImageDTO imageDTO) throws IOException {

		Image image = imageMapper.toEntity(imageDTO);

		String imagePath = storageService.save(imageStream, imageUploadPath, imageDTO.getImageName());
		String thumbnailPath = storageService.save(thumbnailStream, imageUploadPath, imageDTO.getImageThumbnailName());
		image.setPath(imagePath);
		image.setThumbnailPath(thumbnailPath);

		Image savedImage = imageRepository.save(image);
		
		return savedImage;
	}

	private InputStream createThumbnail(MultipartFile orginalFile, Integer width) throws IOException {

		ByteArrayOutputStream thumbOutputStream = new ByteArrayOutputStream();

		BufferedImage thumbImg = null;
		BufferedImage img = ImageIO.read(orginalFile.getInputStream());
		thumbImg = Scalr.resize(img, Scalr.Method.AUTOMATIC, Scalr.Mode.AUTOMATIC, width, Scalr.OP_ANTIALIAS);
		ImageIO.write(thumbImg, orginalFile.getContentType().split("/")[1], thumbOutputStream);

		InputStream thumbInputStream = new ByteArrayInputStream(thumbOutputStream.toByteArray());

		return thumbInputStream;
	}

	private String generateImageName(Long locationId, Long camId, TimestampDTO timestampDTO, String contentType,
			Boolean isThumbnail) throws Exception {

		StringBuilder sb = new StringBuilder();

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmssSSS");
		String formatDateTime = timestampDTO.getLocalDateTime().format(formatter);

		sb.append(formatDateTime);
		sb.append("_");
		sb.append(locationId);
		sb.append("_");
		sb.append(camId);

		if (isThumbnail) {
			sb.append("_thumbnail");
		}

		if (!contentType.startsWith("image")) {
			throw new Exception("File content type must begin with image");
		}

		if (contentType.equals("image/jpeg")) {
			sb.append(".jpeg");
		}

		return sb.toString();
	}

	private TimestampDTO generateTimestamp(LocationWithAuthDTO locationDTO) throws Exception {

		if (locationDTO.getTimezone() == null) {
			throw new FailedDependencyException("Unable to generate timezone, timezone not present in station record");
		}

		ZoneId zoneId = ZoneId.of(locationDTO.getTimezone());
		ZonedDateTime zonedDateTime = ZonedDateTime.now(zoneId);
		LocalDateTime localDateTime = zonedDateTime.toLocalDateTime();
		Instant timestampInstant = localDateTime.atZone(ZoneId.of("Etc/GMT")).toInstant();

		TimestampDTO timestampDTO = new TimestampDTO();
		timestampDTO.setInstant(timestampInstant);
		timestampDTO.setLocalDateTime(localDateTime);

		return timestampDTO;

	}

	public ImageDetailsDTO getImageDetails(Long imageId)
			throws FailedDependencyException, UserPermissionsException, Exception {
		Optional<Image> optImage = imageRepository.findById(imageId);
		if (!optImage.isPresent()) {
			throw new FailedDependencyException(String.format("No images found with id %d", imageId));
		}
		Image image = optImage.get();

		Long camId = image.getCamId();
		CamDTO camDTO = camService.getCamByPermissions(camId);

		ImageDTO imageDTO = imageMapper.toDTOWithoutPaths(image);

		ImageDetailsDTO imageDetailsDTO = new ImageDetailsDTO();

		imageDetailsDTO.setImageDTO(imageDTO);
		imageDetailsDTO.setCamDTO(camDTO);

		return imageDetailsDTO;
	}

	public byte[] getImage(Long imageId) throws FailedDependencyException, UserPermissionsException, Exception {

		Optional<Image> optImage = imageRepository.findById(imageId);
		if (!optImage.isPresent()) {
			throw new FailedDependencyException(String.format("No images found with id %d", imageId));
		}
		Image image = optImage.get();

		Long camId = image.getCamId();
		camService.getCamByPermissions(camId);

		return storageService.loadImage(imageMapper.toDTO(image));
	}

	public ImageBase64DTO getImageBase64(Long imageId)
			throws FailedDependencyException, UserPermissionsException, Exception {

		Optional<Image> optImage = imageRepository.findById(imageId);
		if (!optImage.isPresent()) {
			throw new FailedDependencyException(String.format("No images found with id %d", imageId));
		}
		Image image = optImage.get();

		Long camId = image.getCamId();
		camService.getCamByPermissions(camId);

		byte[] imageByteArr = storageService.loadImage(imageMapper.toDTO(image));
		String encoded = Base64.getEncoder().encodeToString(imageByteArr);

		ImageBase64DTO imageBase64DTO = new ImageBase64DTO();
		imageBase64DTO.setImage(encoded);
		imageBase64DTO.setImageDetails(imageMapper.toDTO(image));

		return imageBase64DTO;
	}

	public byte[] getThumbnail(Long imageId) throws FailedDependencyException, UserPermissionsException, Exception {

		Optional<Image> optImage = imageRepository.findById(imageId);
		if (!optImage.isPresent()) {
			throw new FailedDependencyException(String.format("No images found with id %d", imageId));
		}
		Image image = optImage.get();

		Long camId = image.getCamId();
		camService.getCamByPermissions(camId);

		return storageService.loadImageThumbnail(imageMapper.toDTO(image));

	}

	public byte[] getRecentThumbnail(Long camId) throws FailedDependencyException, UserPermissionsException, Exception {

		CamDTO cam = camService.getCamByPermissions(camId);

		Location location = locationService.getLocation(cam.getLocation().getId());

		Instant twelveHoursBefore = getTimestampTwelveHoursAgo(location);

		Image image = imageRepository.findFirstByLocationIdAndCamIdAndTimestampAfterOrderByTimestampDesc(
				cam.getLocation().getId(), camId, twelveHoursBefore);

		return storageService.loadImageThumbnail(imageMapper.toDTO(image));

	}

	public List<ThumbnailDTO> getRecentThumbnails(Long locationId) throws IOException {

		List<Roles> roles = appUserService.getRoles();

		Location location = locationService.getLocation(locationId);

		List<Cam> cams = camRepository.findByLocation_IdAndAllowedRoleIn(location.getId(), roles);

		List<ThumbnailDTO> thumbnailsList = new ArrayList<ThumbnailDTO>();

		Instant twelveHoursBefore = getTimestampTwelveHoursAgo(location);

		for (int i = 0; i < cams.size(); i++) {

			ThumbnailDTO thumbnailDTO = new ThumbnailDTO();

			Image image = imageRepository.findFirstByLocationIdAndCamIdAndTimestampAfterOrderByTimestampDesc(
					location.getId(), cams.get(i).getId(), twelveHoursBefore);

			if (image == null) {
				thumbnailDTO.setImageDTO(null);
			} else {
				thumbnailDTO.setImageDTO(imageMapper.toDTO(image));
			}
			thumbnailDTO.setCamDTO(camMapper.toDTO(cams.get(i)));

			thumbnailsList.add(thumbnailDTO);
		}

		return storageService.loadThumbnails(thumbnailsList);
	}
	
	private Instant getTimestampTwelveHoursAgo(Location location) {
		LocalDateTime now = LocalDateTime.now(ZoneId.of(location.getTimezone()));
		now = now.minusHours(12);
		Instant twelveHoursBefore = now.atZone(ZoneId.of("Etc/GMT")).toInstant();

		return twelveHoursBefore;
	}
	
	public PageableWrapper<ThumbnailDTO> getThumbnailsOnSearch(Long camId,  DataSearchTypes source,
			String startDate, String endDate, Pageable pageable) throws UserPermissionsException, IOException, Exception {
		
		CamDTO camDTO = camService.getCamByPermissions(camId);
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmm");
		LocalDateTime start = LocalDateTime.parse(startDate, formatter);
		LocalDateTime end = LocalDateTime.parse(endDate, formatter);

		if(start.isAfter(end)) {
			throw new Exception("Start date is after end date");
		}
		
		if(pageable.getPageSize() >= 25) {
			throw new Exception("Page size must be less than 25");
		}

		ZoneId zoneId = ZoneId.of("Etc/GMT");
		Instant startInstant = start.atZone(zoneId).toInstant();
		Instant endInstant = end.atZone(zoneId).toInstant();
		
		List<Image> images = new ArrayList<Image>();
		int totalRecords = 0;
		
		if(source.equals(DataSearchTypes.ONLY_NON_RETAINED)) {
			totalRecords = imageRepository.countAllByCamIdAndIsRetainedAndTimestampBetween(camDTO.getId(), false, startInstant, endInstant);
			images = imageRepository.findByCamIdAndIsRetainedAndTimestampBetween(camDTO.getId(), false, startInstant, endInstant, pageable);
			
			System.out.println("non retained: "+totalRecords);
			
		} else if (source.equals(DataSearchTypes.ONLY_RETAINED)) {
			totalRecords = imageRepository.countAllByCamIdAndIsRetainedAndTimestampBetween(camDTO.getId(), true, startInstant, endInstant);
			images = imageRepository.findByCamIdAndIsRetainedAndTimestampBetween(camDTO.getId(), true, startInstant, endInstant, pageable);
			
			System.out.println("retained: "+totalRecords);
			
		} else {
			
			totalRecords = imageRepository.countAllByCamIdAndTimestampBetween(camDTO.getId(), startInstant, endInstant);
			images = imageRepository.findByCamIdAndTimestampBetween(camDTO.getId(), startInstant, endInstant, pageable);
		}
		
		List<ThumbnailDTO> thumbnailList = images.stream().map(image -> {
			ThumbnailDTO thumbnailDTO = new ThumbnailDTO();
			thumbnailDTO.setImageDTO(imageMapper.toDTO(image));
			thumbnailDTO.setCamDTO(camDTO);
			return thumbnailDTO;
		}).collect(Collectors.toList());
		
		thumbnailList = storageService.loadThumbnails(thumbnailList);
		
		PageableWrapper<ThumbnailDTO> pageableWrapper = buildPageableWrapper(thumbnailList, pageable, totalRecords);
		
		return pageableWrapper;
	}
	
	private PageableWrapper<ThumbnailDTO> buildPageableWrapper(List<ThumbnailDTO> thumbnailList, Pageable pageable, int totalRecords) {
		
		PageableWrapper<ThumbnailDTO> pageableWrapper = new PageableWrapper<ThumbnailDTO>();
		
		pageableWrapper.setContent(thumbnailList);
		pageableWrapper.setPage(pageable.getPageNumber());
		pageableWrapper.setTotalElements(totalRecords);
		pageableWrapper.setPageSize(pageable.getPageSize());
		if (pageable.getPageNumber() == 0) {
			pageableWrapper.setFirst(true);
		}
		if ((pageable.getPageNumber() + 1) * pageable.getPageSize() >= totalRecords) {
			pageableWrapper.setLast(true);
		}
		
		int totalPages = (totalRecords / pageable.getPageSize());
		if(totalRecords % pageable.getPageSize() != 0) {
			totalPages ++;
		}
		
		pageableWrapper.setTotalPages(totalPages);
		
		String sortStr = pageable.getSort().toString();
		String[] sortArr = sortStr.split(":");
		if(sortArr.length >= 2) {
			pageableWrapper.setSortField(sortArr[0].trim());
			pageableWrapper.setSortDirection(Direction.fromString(sortArr[1].trim()));
		}

		return pageableWrapper;
	}

	public void retainImage(Long imageId) throws Exception {

		Optional<Image> optImage = imageRepository.findById(imageId);

		if (!optImage.isPresent()) {
			throw new Exception(String.format("No images found with id %d", imageId));
		}
		Image image = optImage.get();

		image.setIsRetained(true);
		imageRepository.save(image);
	}

	public void unretainImage(Long imageId) throws Exception {

		Optional<Image> optImage = imageRepository.findById(imageId);

		if (!optImage.isPresent()) {
			throw new Exception(String.format("No images found with id %d", imageId));
		}
		Image image = optImage.get();

		image.setIsRetained(false);
		imageRepository.save(image);
	}

	@Transactional
	public LocationDTO deleteImage(Long imageId) throws IOException {

		Optional<Image> optImage = imageRepository.findById(imageId);
		if(optImage.isEmpty()) {
			throw new FailedDependencyException(String.format("No images found with id %d", imageId));
		}
		Image image = optImage.get();
		
		Location location = locationService.getLocation(image.getLocationId());
		
		storageService.deleteImageAndThumbnail(imageMapper.toDTO(image));

		imageRepository.delete(image);
		
		return locationMapper.toDTO(location);
		
	}

}
