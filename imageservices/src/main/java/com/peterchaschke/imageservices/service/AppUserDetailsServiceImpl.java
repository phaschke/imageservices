package com.peterchaschke.imageservices.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import com.peterchaschke.imageservices.entity.AppUser;
import com.peterchaschke.imageservices.entity.Role;
import com.peterchaschke.imageservices.model.Roles;
import com.peterchaschke.imageservices.repo.AppUserRepository;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Service
public class AppUserDetailsServiceImpl implements UserDetailsService {
	
	@Autowired
	private AppUserRepository appUserRepository;

	public AppUserDetailsServiceImpl(AppUserRepository appUserRepository) {
		this.appUserRepository = appUserRepository;
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		AppUser appUser = appUserRepository.findByUsername(username);

		if (appUser == null) {
			throw new UsernameNotFoundException(username);
		}

		return new User(appUser.getUsername(), appUser.getPassword(), appUser.getEnabled(), true, true, true,
				getAuthorities(appUser.getRoles()));
	}
	
	private Collection<? extends GrantedAuthority> getAuthorities(Collection<Role> roles) {
		List<GrantedAuthority> authorities = new ArrayList<>();
		for (Role role : roles) {
			authorities.add(new SimpleGrantedAuthority(role.getName()));
			role.getPrivileges().stream().map(p -> new SimpleGrantedAuthority(p.getName())).forEach(authorities::add);
		}

		return authorities;
	}
	
	public List<Roles> getRoles() {
		
		List<Roles> roles = new ArrayList<Roles>();
		
		roles.add(Roles.ROLE_GUEST);

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		
		if (auth != null && auth.getAuthorities().stream().anyMatch(a -> a.getAuthority().equals("ROLE_ADMIN"))) {

			roles.add(Roles.ROLE_ADMIN);
			roles.add(Roles.ROLE_USER);
		}
		if (auth != null && auth.getAuthorities().stream().anyMatch(a -> a.getAuthority().equals("ROLE_USER"))) {

			roles.add(Roles.ROLE_USER);
		}
		
		return roles;
	}
	
	

}
