package com.peterchaschke.imageservices.service;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.peterchaschke.imageservices.FailedDependencyException;
import com.peterchaschke.imageservices.UserAuthorizationException;
import com.peterchaschke.imageservices.entity.Cam;
import com.peterchaschke.imageservices.entity.Location;
import com.peterchaschke.imageservices.mapper.LocationMapper;
import com.peterchaschke.imageservices.model.LocationDTO;
import com.peterchaschke.imageservices.model.LocationWithAuthDTO;
import com.peterchaschke.imageservices.model.Roles;
import com.peterchaschke.imageservices.model.WeatherStationDTO;
import com.peterchaschke.imageservices.repo.CamRepository;
import com.peterchaschke.imageservices.repo.LocationRepository;

@Service
public class LocationService {

	@Autowired
	private LocationRepository locationRepository;
	
	@Autowired
	private LocationMapper locationMapper;
	
	@Autowired
	private CamRepository camRepository;
	
	@Autowired
	private AppUserDetailsServiceImpl appUserService;

	public Location getLocation(Long locationId) throws FailedDependencyException {

		Location location = locationRepository.findByIdAndActive(locationId, true);

		if (location == null) {
			throw new FailedDependencyException("Location does not exist");
		}

		return location;
	}
	
	public LocationDTO getLocationDetails(Long locationId) {
		
		Location location = getLocation(locationId);
		
		// Check permissions
		List<Roles> roles = appUserService.getRoles();
		
		Collection<Cam> cams = camRepository.findByLocation_IdAndAllowedRoleIn(locationId, roles);
		
		location.setCams(cams);
		
		return locationMapper.toDTO(location);
	}
	
	public LocationDTO getLocationByWeatherStationId(Long weatherStationId) {
		
		Location location = locationRepository.findByWeatherStationIdAndActive(weatherStationId, true);
		
		if(location == null) {
			throw new FailedDependencyException("No image location could be found with provided weather station id");
		}
		
		return locationMapper.toDTO(location);
	}
	
	public WeatherStationDTO getWeatherStationIdByLocationId(Long locationId) {
		
		Optional<Location> optLocation = locationRepository.findById(locationId);
		
		if(optLocation.isEmpty()) {
			throw new FailedDependencyException(String.format("No image location could be found with provided location id: %d", locationId));
		}
		
		Location location = optLocation.get();
		
		WeatherStationDTO weatherStationDTO = new WeatherStationDTO();
		weatherStationDTO.setId(location.getWeatherStationId());
		
		return weatherStationDTO;
	}

	public void authorizeDataRequest(String user, String userKey, LocationWithAuthDTO locationDTO) throws Exception {

		if (requiredApiKeysExist(user, userKey, locationDTO)) {
			throw new UserAuthorizationException("API key and/or API user not present in request");
		}

		// Make sure API User has permissions for requested station
		if (locationDTO.getUser().compareTo(user) != 0) {
			throw new UserAuthorizationException("API User does not have permissions for the requested station");
		}

		// Compare stored key with given key
		if (locationDTO.getUserKey().compareTo(userKey) != 0) {
			throw new UserAuthorizationException("API Key mismatch");
		}

	}

	private boolean requiredApiKeysExist(String user, String userKey, LocationWithAuthDTO locationDTO) {
		return (user == null || userKey == null || locationDTO == null);
	}

}
