package com.peterchaschke.imageservices.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.peterchaschke.imageservices.FailedDependencyException;
import com.peterchaschke.imageservices.UserPermissionsException;
import com.peterchaschke.imageservices.entity.Cam;
import com.peterchaschke.imageservices.mapper.CamMapper;
import com.peterchaschke.imageservices.model.CamDTO;
import com.peterchaschke.imageservices.model.Roles;
import com.peterchaschke.imageservices.repo.CamRepository;

@Service
public class CamService {
	
	@Autowired
	private CamRepository camRepository;
	
	@Autowired
	private CamMapper camMapper;
	
	@Autowired
	private AppUserDetailsServiceImpl appUserService;
	
	public Cam getActiveCamById(Long camId) throws FailedDependencyException {
		
		Cam cam = camRepository.findByIdAndActive(camId, true);
		
		if (cam == null) {
			throw new FailedDependencyException("Requested cam does not exist.");
		}
		
		return cam;
	}
	
	public CamDTO getCamByPermissions(Long camId) throws FailedDependencyException, UserPermissionsException {
		
		List<Roles> roles = appUserService.getRoles();

		Cam cam = camRepository.findByIdAndActive(camId, true);
		
		if (camId == null) {
			throw new FailedDependencyException("Cam does not exist.");
		}
		
		if(!roles.contains(cam.getAllowedRole())) {
			throw new UserPermissionsException("You do not have appropriate permissions to view this cam");
		}
		
		return camMapper.toDTO(cam);
		
	}

}
