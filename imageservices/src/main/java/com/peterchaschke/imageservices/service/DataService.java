package com.peterchaschke.imageservices.service;

import java.io.IOException;
import java.nio.file.Path;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.peterchaschke.imageservices.entity.Image;
import com.peterchaschke.imageservices.mapper.ImageMapper;
import com.peterchaschke.imageservices.model.ImageDTO;
import com.peterchaschke.imageservices.repo.ImageRepository;
import com.peterchaschke.imageservices.storage.StorageService;

@EnableScheduling
@EnableAsync
@Service
public class DataService {
	
	@Autowired
	private ImageRepository imageRepository;
	
	@Autowired
	private StorageService storageService;
	
	@Autowired
	private ImageMapper imageMapper;
	
	private static final Logger logger = LoggerFactory.getLogger(DataService.class);
	
	@Value("${retention.days}")
	private int retentionDays;
	
	@Async
	//@Scheduled(cron = "0 * * * * *")
	@Scheduled(cron = "${cron.data.retention}")
	protected void tuncateData() {
		
		logger.info("[Data Truncation] Begin data truncation");
		
		ZoneId z = ZoneId.of("Etc/GMT");
		LocalDate today = LocalDate.now(z);
		LocalDate dateToRetention = today.minusDays(retentionDays);
		
		LocalDateTime dateTimeRetentionCutoff = dateToRetention.atStartOfDay();
		
		Instant instantRetentionCutoff = dateTimeRetentionCutoff.atZone(ZoneId.of("Etc/GMT")).toInstant();
		
		List<Image> imagesToDelete = imageRepository.findByIsRetainedAndTimestampBefore(false, instantRetentionCutoff);
		
		logger.info(String.format("[Data Truncation] Found %d image(s) past the retention date to remove", imagesToDelete.size()));
		
		imagesToDelete.forEach(image -> {
			
			try {
				
				storageService.deleteImageAndThumbnail(imageMapper.toDTO(image));
				imageRepository.delete(image);
				
			} catch (IOException e) {
				// Skip image
				logger.error(String.format("[Data Truncation] Failed to delete image with id: %d: %s", image.getId(), e.getMessage()));
			}
		});
		
		logger.info("[Data Truncation] End data truncation");
	}
	
	@Async
	@Scheduled(cron = "${cron.data.validation.dbfs}")
	protected void verifyDataIntegrityDb() {
		
		logger.info("[Data Validation DB->FS] Begin verifying data integrity");
		
		List<Image> images  = imageRepository.findAll();
		
		logger.info(String.format("[Data Validation DB->FS] Verifying %d image(s) for db -> file system integrity", images.size())); 
		
		int numberOfRemovedImages = 0;
		
		for(int i = 0; i < images.size(); i++) {
			Image image = images.get(i);
			
			ImageDTO imageDTO = imageMapper.toDTO(image);
			
			if(!storageService.imageExists(imageDTO)) {
				// Image not found. Remove any thumbnails and db record
				try {
					storageService.deleteImageAndThumbnail(imageDTO);
					numberOfRemovedImages++;
					imageRepository.delete(image);
					
				} catch (IOException e) {
					
					logger.error(String.format("[Data Validation DB->FS] Failed to removed image with id %d from the file system: %s", image.getId(), e.getMessage())); 
					
				}
				
			}
		}
		
		logger.info(String.format("[Data Validation DB->FS] Removed %d image(s) present in the db but not in the file system", numberOfRemovedImages)); 
		logger.info("[Data Validation DB->FS] End verifying data integrity");
		
	}
	
	@Async
	@Scheduled(cron = "${cron.data.validation.fsdb}")
	protected void verifyDataIntegrityFs() {
		
		logger.info("[Data Validation FS->DB] Begin verifying data integrity");
		
		try {
			List<Path> imagePathsToCheck = storageService.listFiles();
			checkFilesAgainstDb(imagePathsToCheck);
			
		} catch (IOException e) {
			
			logger.error(String.format("[Data Validation FS->DB] Failed to get list of image files: %s", e.getMessage())); 
			
		}
		
		logger.info("[Data Validation FS->DB] End verifying data integrity");
	}
	
	private void checkFilesAgainstDb(List<Path> imagePathsToCheck) {
		
		logger.info(String.format("[Data Validation FS->DB] Checking %d files against the db", imagePathsToCheck.size()));
		
		int numberOfRemovedImages = 0;
		
		for(int i = 0; i < imagePathsToCheck.size(); i++) {
			
			Path path = imagePathsToCheck.get(i);
			
			String imageName = path.getFileName().toString();
			
			Optional<Image> image = Optional.empty();
			
			if(imageName.endsWith("thumbnail.jpeg")) {
				image = imageRepository.findByImageThumbnailName(imageName);
			} else {
				image = imageRepository.findByImageName(imageName);
			}
			
			if(image.isEmpty()) {
				
				try {
					storageService.deleteByPath(path);
					numberOfRemovedImages++;
					
				} catch (IOException e) {
					
					logger.error(String.format("[Data Validation FS->DB] Failed to removed image with path %s from the file system: %s", path.toString(), e.getMessage())); 
				}
				
			}
		}
		
		logger.info(String.format("[Data Validation FS->DB] Removed %d image(s) present in the file system but not in the db", numberOfRemovedImages)); 
		
	}
	
	@Async
	@Scheduled(cron = "${cron.data.cleanfs}")
	protected void removeEmptyDirectories() {
		
		logger.info("[Clean Directories] Begin removing empty directories");
		
		try {
			storageService.removeEmptyDirectories();
			
		} catch (IOException e) {
			
			logger.error(String.format("Failed to remove empty directories: %s", e.getMessage())); 
		}
		
		logger.info("[Clean Directories] End removing empty directories");
	}

}
