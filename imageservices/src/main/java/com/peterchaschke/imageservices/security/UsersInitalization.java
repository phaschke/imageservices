package com.peterchaschke.imageservices.security;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.peterchaschke.imageservices.entity.AppUser;
import com.peterchaschke.imageservices.entity.Privilege;
import com.peterchaschke.imageservices.entity.Role;
import com.peterchaschke.imageservices.repo.AppUserRepository;
import com.peterchaschke.imageservices.repo.PrivilegeRepository;
import com.peterchaschke.imageservices.repo.RoleRepository;

@Component
public class UsersInitalization implements ApplicationListener<ContextRefreshedEvent> {
	
boolean alreadySetup = false;

	@Value("${users.user.name}")
	private String userName;
	
	@Value("${users.user.password}")
	private String userPassword;
	
	@Value("${users.admin.name}")
	private String adminName;
	
	@Value("${users.admin.password}")
	private String adminPassword;
	
	@Autowired
    private AppUserRepository appUserRepository;
	
	@Autowired
    private RoleRepository roleRepository;
	
	@Autowired
	private PrivilegeRepository privilegeRepository;
	
	@Autowired
    private PasswordEncoder encoder;

	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		
		if(alreadySetup) return;
		
		Privilege viewPrivilege = createPrivilegeIfNotFound("VIEW_PRIVILEGE");
		Privilege markPrivilege = createPrivilegeIfNotFound("MARK_PRIVILEGE");
		
		List<Privilege> adminPrivileges = Arrays.asList(viewPrivilege, markPrivilege); 
		
		createRoleIfNotFound("ROLE_ADMIN", adminPrivileges);
        createRoleIfNotFound("ROLE_USER", Arrays.asList(viewPrivilege));
        //createRoleIfNotFound("ROLE_GUEST",  Arrays.asList());
        
        // Create admin user
        AppUser adminUser = appUserRepository.findByUsername(adminName);
        
        if(adminUser == null) {
        	
        	adminUser = new AppUser();
        	adminUser.setUsername(adminName);
        	adminUser.setPassword(encoder.encode(adminPassword));
        }
        
        Role adminRole = roleRepository.findByName("ROLE_ADMIN");
        
        adminUser.setRoles(Arrays.asList(adminRole));
        adminUser.setEnabled(true);
        
        appUserRepository.save(adminUser);
        
        // Create user
        AppUser user = appUserRepository.findByUsername(userName);
        
        if(user == null) {
        	
        	user = new AppUser();
        	user.setUsername(userName);
        	user.setPassword(encoder.encode(userPassword));
        }
        
        Role userRole = roleRepository.findByName("ROLE_USER");
        
        user.setRoles(Arrays.asList(userRole));
        user.setEnabled(true);
        
        appUserRepository.save(user);

        alreadySetup = true;
		
	}
	
	@Transactional
    Privilege createPrivilegeIfNotFound(String name) {
 
        Privilege privilege = privilegeRepository.findByName(name);
        if (privilege == null) {
            privilege = new Privilege(name);
            privilegeRepository.save(privilege);
        }
        return privilege;
    }
	
	@Transactional
    Role createRoleIfNotFound(String name, Collection<Privilege> privileges) {
 
        Role role = roleRepository.findByName(name);
        if (role == null) {
        	
            role = new Role(name);
            role.setPrivileges(privileges);
            roleRepository.save(role);
        }
        return role;
    }
}
