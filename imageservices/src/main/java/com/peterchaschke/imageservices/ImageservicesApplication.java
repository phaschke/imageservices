package com.peterchaschke.imageservices;

import javax.annotation.Resource;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.peterchaschke.imageservices.storage.FilesStorageServiceImpl;

@SpringBootApplication
public class ImageservicesApplication implements CommandLineRunner {

	@Resource
	FilesStorageServiceImpl storageService;

	public static void main(String[] args) {
		SpringApplication.run(ImageservicesApplication.class, args);
	}

	@Override
	public void run(String... arg) throws Exception {
		storageService.init();
	}

}
