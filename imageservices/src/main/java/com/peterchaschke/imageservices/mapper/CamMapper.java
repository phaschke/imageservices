package com.peterchaschke.imageservices.mapper;

import org.mapstruct.Mapper;
import com.peterchaschke.imageservices.entity.Cam;
import com.peterchaschke.imageservices.model.CamDTO;

@Mapper(componentModel = "spring")
public interface CamMapper {
	
	CamDTO toDTO(Cam cam);
	Cam toEntity(CamDTO camDto);
}
