package com.peterchaschke.imageservices.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.peterchaschke.imageservices.entity.Location;
import com.peterchaschke.imageservices.model.LocationDTO;
import com.peterchaschke.imageservices.model.LocationWithAuthDTO;

@Mapper(componentModel = "spring")
public interface LocationMapper {
	
	LocationWithAuthDTO toDTOWithAuth(Location location);
	
	Location toEntityFromAuth(LocationWithAuthDTO locationDTO);
	
	LocationDTO toDTO(Location location);
	@Mapping(target = "user", ignore = true)
	@Mapping(target = "userKey", ignore = true)
	Location toEntity(LocationDTO locationDTO);
	
	LocationDTO toDTOFromDTOWithAuth(LocationWithAuthDTO locationDTO);
	
}
