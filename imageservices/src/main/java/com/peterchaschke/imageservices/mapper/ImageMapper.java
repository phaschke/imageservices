package com.peterchaschke.imageservices.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.peterchaschke.imageservices.entity.Image;
import com.peterchaschke.imageservices.model.ImageDTO;

@Mapper(componentModel = "spring")
public interface ImageMapper {
	
	ImageDTO toDTO(Image image);
	Image toEntity(ImageDTO image);
	
	@Mapping(target = "path", ignore = true)
	@Mapping(target = "thumbnailPath", ignore = true)
	ImageDTO toDTOWithoutPaths(Image image);
}
