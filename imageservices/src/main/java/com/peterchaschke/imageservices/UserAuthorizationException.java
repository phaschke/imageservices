package com.peterchaschke.imageservices;

public class UserAuthorizationException extends RuntimeException {

	private static final long serialVersionUID = 3270778785679339443L;
	
	public UserAuthorizationException(String string) {
		super(string);
	}
}
