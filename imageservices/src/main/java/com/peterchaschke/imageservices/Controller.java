package com.peterchaschke.imageservices;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.peterchaschke.imageservices.model.CamDTO;
import com.peterchaschke.imageservices.model.DataSearchTypes;
import com.peterchaschke.imageservices.model.ImageBase64DTO;
import com.peterchaschke.imageservices.model.ImageDTO;
import com.peterchaschke.imageservices.model.ImageDetailsDTO;
import com.peterchaschke.imageservices.model.IncomingFileDetailsRequestDTO;
import com.peterchaschke.imageservices.model.LocationDTO;
import com.peterchaschke.imageservices.model.PageableWrapper;
import com.peterchaschke.imageservices.model.ThumbnailDTO;
import com.peterchaschke.imageservices.model.WeatherStationDTO;
import com.peterchaschke.imageservices.service.CamService;
import com.peterchaschke.imageservices.service.ImageServiceImpl;
import com.peterchaschke.imageservices.service.LocationService;

@RestController
@RequestMapping("/api/images")
public class Controller {

	@Autowired
	private final ImageServiceImpl imageService;
	private final LocationService locationService;
	private final CamService camService;

	Controller(ImageServiceImpl imageService, LocationService locationService, CamService camService) {
		this.imageService = imageService;
		this.locationService = locationService;
		this.camService = camService;
	}

	@GetMapping("/location/{locationId}")
	public ResponseEntity<?> getLocationDetails(@PathVariable Long locationId) {

		LocationDTO locationDTO = locationService.getLocationDetails(locationId);
		return new ResponseEntity<LocationDTO>(locationDTO, HttpStatus.OK);
	}

	@GetMapping("/cam/{camId}")
	public ResponseEntity<?> getCamDetails(@PathVariable Long camId) {
		
		try {
			CamDTO camDTO = camService.getCamByPermissions(camId);
			return new ResponseEntity<CamDTO>(camDTO, HttpStatus.OK);
			
		} catch (FailedDependencyException e) {
			
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.FAILED_DEPENDENCY);
			
		} catch (UserPermissionsException ex) {
			
			return new ResponseEntity<String>(ex.getMessage(), HttpStatus.FORBIDDEN);
		}
		
	}
	
	@GetMapping("/location/weather-station/{weatherStationId}")
	public ResponseEntity<?> getLocationByWeatherStationId(@PathVariable Long weatherStationId) {
		
		try {
			LocationDTO locationDTO = locationService.getLocationByWeatherStationId(weatherStationId);
			return new ResponseEntity<LocationDTO>(locationDTO, HttpStatus.OK);
			
		} catch (FailedDependencyException e) {
			
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.NOT_FOUND);
		}
		
	}
	
	@GetMapping("/weather-station/location/{locationId}")
	public ResponseEntity<?> getByWeatherStationIdByLocationId(@PathVariable Long locationId) {
		
		try {
			WeatherStationDTO weatherStationDTO = locationService.getWeatherStationIdByLocationId(locationId);
			
			return new ResponseEntity<WeatherStationDTO>(weatherStationDTO, HttpStatus.OK);
			
		} catch (FailedDependencyException e) {
			
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.NOT_FOUND);
		}
		
	}

	@PostMapping("/{locationId}/{camId}")
	public ResponseEntity<?> handleFileUpload(@PathVariable Long locationId, @PathVariable Long camId,
			@RequestPart("imageFile") MultipartFile file,
			@RequestPart("details") IncomingFileDetailsRequestDTO fileDetails) {

		try {
			ImageDTO savedImage = imageService.saveImage(locationId, camId, file, fileDetails);

			return new ResponseEntity<ImageDTO>(savedImage, HttpStatus.OK);

		} catch (FailedDependencyException e) {

			return new ResponseEntity<String>(e.getMessage(), HttpStatus.UNPROCESSABLE_ENTITY);

		} catch (UserAuthorizationException e) {

			return new ResponseEntity<String>(e.getMessage(), HttpStatus.UNAUTHORIZED);

		} catch (Exception e) {

			return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@GetMapping("/image/{imageId}/details")
	public ResponseEntity<?> getImageDetails(@PathVariable Long imageId) {

		try {

			ImageDetailsDTO imageDetailsDTO = imageService.getImageDetails(imageId);

			return new ResponseEntity<ImageDetailsDTO>(imageDetailsDTO, HttpStatus.OK);

		} catch (UserPermissionsException e) {

			return new ResponseEntity<String>(e.getMessage(), HttpStatus.FORBIDDEN);

		} catch (FailedDependencyException ex) {

			return new ResponseEntity<String>(ex.getMessage(), HttpStatus.NOT_FOUND);
			
		} catch (Exception exx) {
			
			return new ResponseEntity<String>(exx.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping(value = "/image/{imageId}", produces = MediaType.IMAGE_JPEG_VALUE)
	public ResponseEntity<?> getImage(@PathVariable Long imageId) {

		try {
			byte[] file = imageService.getImage(imageId);

			return new ResponseEntity<byte[]>(file, HttpStatus.OK);

		} catch (UserPermissionsException e) {

			return new ResponseEntity<String>(e.getMessage(), HttpStatus.FORBIDDEN);

		} catch (FailedDependencyException ex) {

			return new ResponseEntity<String>(ex.getMessage(), HttpStatus.NOT_FOUND);
			
		} catch(Exception exx) {
			
			return new ResponseEntity<String>(exx.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}
	
	@GetMapping("/image/base64/{imageId}")
	public ResponseEntity<?> getImageBase64(@PathVariable Long imageId) {

		try {
			ImageBase64DTO file = imageService.getImageBase64(imageId);

			return new ResponseEntity<ImageBase64DTO>(file, HttpStatus.OK);

		} catch (UserPermissionsException e) {

			return new ResponseEntity<String>(e.getMessage(), HttpStatus.FORBIDDEN);

		} catch (FailedDependencyException ex) {

			return new ResponseEntity<String>(ex.getMessage(), HttpStatus.NOT_FOUND);
			
		} catch(Exception exx) {
			
			return new ResponseEntity<String>(exx.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}
	
	@GetMapping(value = "/thumbnails/image/{imageId}", produces = MediaType.IMAGE_JPEG_VALUE)
	public ResponseEntity<?> getThumbnail(@PathVariable Long imageId) {
		
		try {
			byte[] file = imageService.getThumbnail(imageId);

			return new ResponseEntity<byte[]>(file, HttpStatus.OK);

		} catch (UserPermissionsException e) {

			return new ResponseEntity<String>(e.getMessage(), HttpStatus.FORBIDDEN);

		} catch (FailedDependencyException ex) {

			return new ResponseEntity<String>(ex.getMessage(), HttpStatus.NOT_FOUND);
			
		} catch(Exception exx) {
			
			return new ResponseEntity<String>(exx.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping(value = "/thumbnails/cam/{camId}", produces = MediaType.IMAGE_JPEG_VALUE)
	public ResponseEntity<?> getThumbnailByCam(@PathVariable Long camId) {
		
		try {
			byte[] file = imageService.getRecentThumbnail(camId);

			return new ResponseEntity<byte[]>(file, HttpStatus.OK);

		} catch (UserPermissionsException e) {

			return new ResponseEntity<String>(e.getMessage(), HttpStatus.FORBIDDEN);

		} catch (FailedDependencyException ex) {

			return new ResponseEntity<String>(ex.getMessage(), HttpStatus.NOT_FOUND);
			
		} catch(Exception exx) {
			
			return new ResponseEntity<String>(exx.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/thumbnails/location/{locationId}")
	public ResponseEntity<?> getAllRecentThumbnailsByLocation(@PathVariable Long locationId) throws IOException {

		List<ThumbnailDTO> thumbnails = imageService.getRecentThumbnails(locationId);

		return new ResponseEntity<List<ThumbnailDTO>>(thumbnails, HttpStatus.OK);
	}

	@PatchMapping("/image/retain/{imageId}")
	public ResponseEntity<?> retainImage(@PathVariable Long imageId) {

		try {
			imageService.retainImage(imageId);

		} catch (Exception e) {

			return new ResponseEntity<String>(e.getMessage(), HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<>(HttpStatus.OK);
	}

	@PatchMapping("/image/unretain/{imageId}")
	public ResponseEntity<?> unretainImage(@PathVariable Long imageId) {

		try {
			imageService.unretainImage(imageId);

		} catch (Exception e) {

			return new ResponseEntity<String>(e.getMessage(), HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<>(HttpStatus.OK);
	}

	@DeleteMapping("/image/{imageId}")
	public ResponseEntity<?> deleteImage(@PathVariable Long imageId) {
		
		try {
			
			LocationDTO locationDTO = imageService.deleteImage(imageId);
			return new ResponseEntity<LocationDTO>(locationDTO, HttpStatus.OK);
			
		} catch (IOException e) {

			return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
			
		} catch (FailedDependencyException ex) {

			return new ResponseEntity<String>(ex.getMessage(), HttpStatus.NOT_FOUND);
		}

	}
	
	@GetMapping("/search/{camId}")
	public ResponseEntity<?> searchImages(@PathVariable Long camId,
			@RequestParam("startdate") String startDate, @RequestParam("enddate") String endDate, 
			@RequestParam("source") DataSearchTypes source, Pageable pageable) {
		
		try {
			PageableWrapper<ThumbnailDTO> results = imageService.getThumbnailsOnSearch(camId, source, startDate, endDate, pageable);
			
			return new ResponseEntity<PageableWrapper<ThumbnailDTO>>(results, HttpStatus.OK);
			
		} catch (UserPermissionsException e) {

			return new ResponseEntity<String>(e.getMessage(), HttpStatus.FORBIDDEN);
			
		} catch (Exception e) {

			return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
	}
	

}
