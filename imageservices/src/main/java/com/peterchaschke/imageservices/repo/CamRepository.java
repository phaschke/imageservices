package com.peterchaschke.imageservices.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.peterchaschke.imageservices.entity.Cam;
import com.peterchaschke.imageservices.model.Roles;

@Repository
public interface CamRepository extends JpaRepository<Cam, Long> {
	
	Cam findByIdAndActive(Long id, boolean active);
	Cam findByIdAndActiveAndAllowedRoleIn(Long id, boolean active, List<Roles> roles);
	Cam findByIdAndAllowedRoleIn(Long id, List<Roles> roles);
	List<Cam> findByLocation_IdAndAllowedRoleIn(Long id, List<Roles> roles);
	List<Cam> findByLocation_Id(Long id);
}
