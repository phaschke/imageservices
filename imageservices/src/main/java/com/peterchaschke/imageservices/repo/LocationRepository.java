package com.peterchaschke.imageservices.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.peterchaschke.imageservices.entity.Location;

@Repository
public interface LocationRepository extends JpaRepository<Location, Long> {
	
	Location findByIdAndActive(Long id, boolean active);
	Location findByWeatherStationIdAndActive(Long weatherStationId, boolean active);
}
