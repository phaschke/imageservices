package com.peterchaschke.imageservices.repo;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.peterchaschke.imageservices.entity.Image;

@Repository
public interface ImageRepository extends JpaRepository<Image, Long> {
	
	Image findFirstByLocationIdAndCamIdAndTimestampAfterOrderByTimestampDesc(Long locationId, Long camId, Instant twelveHoursBefore);
	List<Image> findByIsRetainedAndTimestampBefore(boolean retained, Instant timestamp);
	Optional<Image> findByImageName(String imageName);
	Optional<Image> findByImageThumbnailName(String imageThumbnailName);
	List<Image> findByCamIdAndTimestampBetween(Long camId, Instant startTimestamp, Instant endTimestamp, Pageable pageable);
	int countAllByCamIdAndTimestampBetween(Long camId, Instant startTimestamp, Instant endTimestamp);
	List<Image> findByCamIdAndIsRetainedAndTimestampBetween(Long camId, boolean retained, Instant startTimestamp, Instant endTimestamp, Pageable pageable);
	int countAllByCamIdAndIsRetainedAndTimestampBetween(Long camId, boolean retained, Instant startTimestamp, Instant endTimestamp);

}
