#ifndef _ESP32CAM.H
#define _ESP32CAM.H

// WiFi Connection
#define SSIDD ""
#define PASSWORD ""

// Image Service Connection
#define HOST ""
#define HTTPS_PORT 443
#define REQUEST_URL ""
#define API_USER ""
#define API_KEY ""

#endif //_ESP32CAM.H
